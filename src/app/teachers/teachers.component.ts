import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private location: Location) {}

  ngOnInit(): void {}
  
  goBack(): void {
    this.location.back();
  }


}