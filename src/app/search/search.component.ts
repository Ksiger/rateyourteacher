import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {startWith, map} from 'rxjs/operators';


export interface schoolGroup {
  letter: string;
  names: string[];
}

export const _filter = (opt: string[], value: string): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})


export class SearchComponent implements OnInit{
  

  schoolForm: FormGroup = this._formBuilder.group({
    schoolGroup: '',
  });

  schoolGroups: schoolGroup[] = [{
    letter: 'B',
    names: ['Adolf-Kolping-Berufsschule',
       'Berufsschule für Büromanagement und Industriekaufleute',
       'Berufsschule für Medienberufe',
       'Berufsschule für den Einzelhandel Nord',
       'Berufsschule für Steuern',
       'Berufsschule für Informationstechnik',
    ]
 }, {
    letter: 'I',
    names: ['Isar-Fachoberschule  München',
       'Isar-Grundschule (Isartor)',
       'Isar-Grundschule (Obersendling)',
       'Isar-Gymnasium (Huber) München',
       'Isar-Gymnasium München',
       'Isar-Mittelschule München',
       'Isar-Realschule (Huber) München',
       'Isar-Realschule München',
       'Isar-Wirtschaftsschule München'
    ]
 }];

  schoolGroupOptions!: Observable<schoolGroup[]>;

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.schoolGroupOptions = this.schoolForm.get('schoolGroup')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );
  }

  private _filterGroup(value: string): schoolGroup[] {
    if (value) {
      return this.schoolGroups
        .map(group => ({letter: group.letter, names: _filter(group.names, value)}))
        .filter(group => group.names.length > 0);
    }

    return this.schoolGroups;
  }
}